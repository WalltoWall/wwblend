##WWBLEND
A jQuery plugin to simulate blend modes.

Right now it is supposed to work on the site's background image.  Plans
to expand it in the near future.

Currently supported: 
* Multiply

##Usage
$(ELEMENT).wwblend({
canvas:{
  width: ##,
  height: ##
},
color: {
  r: ###,
  g: ###,
  b: ###
},
image:{
  src: ""
}
}
});

* image.src is the url of the image to blend
* color.r, color.g, color.b are the rgb values of the color that is
  blending with the image
* canvas.width, canvas.height are the width and height of the canvas
  being blended



