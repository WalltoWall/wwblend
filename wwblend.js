(function($){
  colorCache = {};
  $.fn.wwblend = function(options){
    var defaults = {
      canvas: {
        left: 0,
        top: 0
      },
      browser: {
        ratio: 0
      },
      image:{
        ratio: 0
      },
      context:{}
    };

    var element = $(this);
    var img;
    var loaded = false;
    var canvas;
    var drawn = false;

    options = $.extend(defaults, options);

    this.initialize = function(){
      colorCache[options.color.r] = {};
      colorCache[options.color.g] = {};
      colorCache[options.color.b] = {};

      img = new Image();
      img.onload = function(){
        imageLoaded();
      };
      img.src = options.image.src;
      waitForImage();
    };

    var imageLoaded = function(){
      loaded = true;
    };

    var waitForImage = function(){
      if(loaded){
        setUpCanvas();
      }else{
        setTimeout(function(){waitForImage();}, 100);
      }
    };

    var setUpCanvas = function(){
      canvas = document.createElement('canvas');
      $(canvas).attr('width', options.canvas.width);
      $(canvas).attr('height', options.canvas.height);
      $(canvas).css('position', 'absolute');
      $(canvas).css('top', options.canvas.top+"px");
      $(canvas).css('left', options.canvas.left+"px");

      $(img).css('position', 'absolute');
      $(img).css('left', '-10000px');
      $('body').append($(img)).each(function(){
        calculateScale(function(){
          $(element).append($(canvas)).each(function(){
            drawCanvas();
            $(window).bind('resize', function(){
              calculateScale(function(){drawCanvas();});
            });
            $(window).bind('scroll', function(d,e,f){
              calculateScale(function(){drawCanvas();});
            });
          });
        });
      });
    };

    var calculateScale = function(cb){
      options.image.ratio = $(img).width()/$(img).height();
      options.browser.ratio = window.innerWidth/window.innerHeight;
      if(options.image.ratio > options.browser.ratio){
        options.context.width = window.innerHeight*options.image.ratio;
        options.context.height = window.innerHeight;
      }else if(options.image.ratio < options.browser.ratio){
        options.context.width = window.innerWidth;
        options.context.height = window.innerWidth / options.image.ratio;
      }else{
        options.context.width = window.innerWidth;
        options.context.height = window.innerHeight;
      }
      cb();
    };

    var drawCanvas = function(){
      context = $(canvas)[0].getContext('2d');
      context.drawImage(img, -($(canvas).offset().left), -($(canvas).offset().top-$(window).scrollTop()), options.context.width, options.context.height);
      imgData = context.getImageData(0, 0, $(canvas).width(), $(canvas).height());

      pix = imgData.data;

      for(var i = 0, n=pix.length; i < n; i+=4){
        pix[i] = multiplyPixels(pix[i], options.color.r);
        pix[i+1] = multiplyPixels(pix[i+1], options.color.g);
        pix[i+2] = multiplyPixels(pix[i+2], options.color.b);
      }
  
      imgData.data = pix;
      context.putImageData(imgData, 0, 0);

      if(!drawn){
        drawn = true;
        if(options.afterDraw){
          setTimeout(function(){animateIn();},options.afterDraw.delay);
        }
      }
    };
    var animateIn = function(){
      $(element).animate({left: options.afterDraw.left},{
        duration:options.afterDraw.speed, 
        step:function(){drawCanvas();},
        complete:function(){drawCanvas();}
      });
    };

    var multiplyPixels = function(top, bottom){
      if(!colorCache[bottom][top]){
        colorCache[bottom][top] = top*bottom/255;
        return colorCache[bottom][top];
      }
      return colorCache[bottom][top];
    };

    return this.initialize();
  };
})(jQuery);
